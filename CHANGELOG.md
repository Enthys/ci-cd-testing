## [1.0.3](https://gitlab.com/Enthys/ci-cd-testing/compare/v1.0.2...v1.0.3) (2024-04-26)


### Bug Fixes

* another ([29e79d3](https://gitlab.com/Enthys/ci-cd-testing/commit/29e79d3a14484abea26dd788deb701f6963d3b36))
* asddsa ([1628d98](https://gitlab.com/Enthys/ci-cd-testing/commit/1628d984fe4502e22cd510ff88bafc4f6d65583d))
* one ([87307df](https://gitlab.com/Enthys/ci-cd-testing/commit/87307df783fe15d87e3f9765e0ce9c6fa6708fd5))
* removed unnecessary npm package ([c455ce5](https://gitlab.com/Enthys/ci-cd-testing/commit/c455ce5859bd25a5e4f0f0a7d60f76410152fb50))
* three ([7368bfb](https://gitlab.com/Enthys/ci-cd-testing/commit/7368bfb057c96008e18d89f3d5314a1f7929c860))
* two ([095daf8](https://gitlab.com/Enthys/ci-cd-testing/commit/095daf8954eaba1bb31612c9560c88582a4cbbce))

## [1.0.2](https://gitlab.com/Enthys/ci-cd-testing/compare/v1.0.1...v1.0.2) (2024-04-26)


### Bug Fixes

* hoping to get changelog 2 ([a417e05](https://gitlab.com/Enthys/ci-cd-testing/commit/a417e05762b43dae02befdf439c9c090a490d436))
